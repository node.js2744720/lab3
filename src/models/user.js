const mongoose = require('mongoose');
const validator = require('validator');

const express = require('express');
const router = express.Router();

router.get('/test', (req, res) => {
    res.send("From a new File");
})
module.exports = router;

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    age: {
        type: Number,
        default: 0,
        validate: {
            validator: function(value) {
                return value >= 0;
            },
            message: 'Age must be a positive number'
        }
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: {
            validator: function (value) {
                return validator.isEmail(value);
            },
            message: props => `${props.value} is not a valid email address!`
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        validate: {
            validator: function (value) {
                return !value.toLowerCase().includes('password');
            },
            message: props => `Password cannot contain the word "password"!`
        }
    }
});
const User = mongoose.model('User', userSchema);
module.exports = User;