const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const app = express();

app.get("/tasks", async (req, res) => {
    try {
        const tasks = await Task.find({});
        res.status(200).send(tasks);
    } catch (error) {
        console.error(error)    ;
        res.status(500).send('Internal Server Error');
    }
});

app.get('/tasks/:id', async (req, res) => {
    const taskId = req.params.id;
    try {
        const task = await Task.findById(taskId);
        if (!task) {
            return res.status(404).json({ message: 'Task not found' });
        }
        res.status(200).json(task);
    } catch (error) {
        console.error(error);
        res.status(500).send('Internal Server Error');
    }
});

app.post('/tasks', async (req, res) => {
    const taskData = req.body;
    const newTask = new Task(taskData);
    try {
        await newTask.save();
        res.status(201).json(newTask);
    } catch (error) {
        console.error(error);
        res.status(500).send('Internal Server Error');
    }
});

app.delete('/task/:id', async (req, res) => {
    const taskId = req.params.id;

    try {
        const task = await Task.findByIdAndDelete(taskId);

        if (!task) {
            return res.status(404).send("Task not found");
        }

        res.status(200).send("Task deleted successfully");
    } catch (error) {
        console.error(error);
        res.status(400).send("Error deleting task");
    }
});

app.delete('/tasks', async (req, res) => {
    try {
        await Task.deleteMany({});
        res.status(200).send("All tasks deleted successfully");
    } catch (error) {
        console.error(error);
        res.status(400).send("Error deleting tasks");
    }
});

app.patch('/tasks/:id', async (req, res) => {
    const taskId = req.params.id;
    const updates = req.body;

    try {
        const task = await Task.findByIdAndUpdate(taskId, updates, { new: true });

        if (!task) {
            return res.status(404).send("Task not found");
        }

        res.status(200).json(task);
    } catch (error) {
        console.error(error);
        res.status(400).send("Error updating task");
    }
});

module.exports = router;