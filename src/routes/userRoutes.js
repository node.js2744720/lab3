const express = require('express');
const router = express.Router();
const User = require('../models/user');
const app = express();

app.get("/users", (req, res) => {
    User.find({})
        .then((users) => {
            res.status(200).send(users);
        })
        .catch((error) => {
            res.status(500).send('Internal Server Error');
        });
});

app.get('/users/:id', (req, res) => {
    const userId = req.params.id;
    User.findById(userId)
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }
            res.status(200).json(user);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});

app.post('/users', (req, res) => {
    const userData = req.body;
    const newUser = new User(userData);
    newUser.save()
        .then(user => {
            res.status(201).json(user);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});

app.delete('/user/:id', async (req, res) => {
    const userId = req.params.id;

    try {
        const user = await User.findByIdAndDelete(userId);

        if (!user) {
            return res.status(404).send("User not found");
        }

        res.status(200).send("User deleted successfully");
    } catch (error) {
        console.error(error);
        res.status(400).send("Error deleting user");
    }
});

app.delete('/users', async (req, res) => {
    try {
        await User.deleteMany({});
        res.status(200).send("All users deleted successfully");
    } catch (error) {
        console.error(error);
        res.status(400).send("Error deleting users");
    }
});

app.patch('/users/:id', async (req, res) => {
    const userId = req.params.id;
    const updates = req.body;

    try {
        const user = await User.findByIdAndUpdate(userId, updates, { new: true });

        if (!user) {
            return res.status(404).send("User not found");
        }

        res.status(200).json(user);
    } catch (error) {
        console.error(error);
        res.status(400).send("Error updating user");
    }
});

router.get('/test', (req, res) => {
    res.send("From a new File");
});

module.exports = router;