const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const User = require('./models/user');

const Task = require('./models/task.js');

const userRoutes = require('./routes/userRoutes');

const taskRoutes = require('./routes/taskRoutes');

const app = express();

dotenv.config();

app.use(express.json());
app.use(userRoutes);
app.use(taskRoutes);

mongoose.connect(process.env.MONGO_URL)
    .then(() => {
        console.log("Connected to MongoDB");
    })
    .catch((error) => {
        console.error("Error connecting to MongoDB:", error);
    });

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
